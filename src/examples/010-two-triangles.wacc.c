#include <stdint.h>

uint32_t values[] = {0x0000ffff, 0xffffffff, 0x00ffffff, 0x0000ffff, 0xffffffff, 0xff00ffff};


uint32_t wacc() {

    static int index = 0;

    return values[(index++) % 6];

}
