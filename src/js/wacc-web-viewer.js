/*

   WACC Web Renderer v0.1.0

   See: https://wacc.rancidbacon.com/

   ----

   MIT License

   WACC Web Renderer
   Copyright (c) 2020 RancidBacon.com

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

 */


function coord_from_int(i) {
    return [(i >>> 24) & 0xff, (i >>> 16) & 0xff];
}


function _convert_color_to_8bit_rgba(value) {
  /*
     Converts array with RGBA color components expressed
     in range 0.0-1.0 into array with component values expressed
     in range 0-255 suitable for use with Canvas `rgba()` function.
  */
  var red = value[0] * 0xff;
  var green = value[1] * 0xff;
  var blue = value[2] * 0xff;
  var alpha = value[3];

  return [red, green, blue, alpha];
}


function color_string_from_float_array(unconverted_color) {
  var [red, green, blue, alpha] = _convert_color_to_8bit_rgba(unconverted_color);
  return `rgba(${red},${green},${blue},${alpha})`;
}


function color_from_int(i) {
  //
  // Note: This returns color component values with a range 0.0-1.0 to
  //       match the functionality of the method with the same
  //       name in the non-web viewer.
  //
  return [((i >>> 10) & 0b11111)/31.0, ((i >>> 5) & 0b11111)/31.0 , ((i >>> 0) & 0b11111)/31.0 , [0.0, 1.0][(i & 0x8000) >>> 15]]
}


function color_string_from_int(i) {
  // Returns a color string suitable for use with Canvas, e.g. 'rgba(255,255,255,1)'.
  var unconverted_color = color_from_int(i);
  return color_string_from_float_array(unconverted_color);
}


var _enable_extra_debug_log = true;

//
// When all three coordinate values of a triangle
// are equal to _FRAME_END_VALUE the triangle is the
// "Special Value" that indicates it is an
// "end-of-frame" marker.
//
// When the first triangle retrieved in a WACC
// is an "end-of-frame" marker it indicates
// that the WACC uses "Frame Mode".
//
// In "Frame Mode" multiple triangles are retrieved
// and displayed per frame. This enables building
// up more complex images than just a single
// triangle. :)
//
// When in "Frame Mode" triangles are retrieved
// and accumulated until an "end-of-frame" marker
// is encountered (or an implementation-defined
// limit on number of triangles is reached).
// Then all accumulated triangles are drawn (in
// the order retrieved).
//
// In non-frame mode (the default) "end-of-frame"
// markers are processed as normal but because
// they have an alpha value of 0 for all three
// coords nothing should be visible.
//
// (Note: this does have the disadvantage that
// a renderer that doesn't support Frame Mode--
// e.g. which might be case early in development--
// will appear to display nothing for the first
// triangle read.)
//
const _FRAME_END_VALUE = 0x00000000;

function _get_next_triangle(wacc_function) {

  var is_frame_end = false;
  var frame_end_value_count = 0;

  var current_result_value;
  var coords = [];
  var colors = [];

  for (let i = 0; i < 3; i++) {
    current_result_value = wacc_function();

    if (_enable_extra_debug_log) {
      console.debug("got WACC data: 0x" + (current_result_value >>> 0).toString(16));
    }

    if (current_result_value == _FRAME_END_VALUE) {
      frame_end_value_count++;
    }

    coords.push(coord_from_int(current_result_value));
    colors.push(color_from_int(current_result_value));
  }

  is_frame_end = (frame_end_value_count == 3);

  return {'coords': coords, 'colors': colors, 'is_frame_end': is_frame_end};
}


var _canvas;
var _ctx;

function draw_triangle(the_triangle) {

  var count = 0;

  for ([x_coord, y_coord] of the_triangle.coords) {

    if (count == 0) {
      _ctx.beginPath();
      _ctx.fillStyle = color_string_from_float_array(the_triangle.colors[0]); // TODO: Handle per-vertex colors better/properly.
      _ctx.strokeStyle = _ctx.fillStyle; // Part of workaround for seam between solid triangles on Canvas. (See below.)
      _ctx.moveTo(x_coord, y_coord)
    }

    _ctx.lineTo(x_coord, y_coord);

    if (_enable_extra_debug_log) {
      console.debug("coord: (%d, %d)", x_coord, y_coord);
    }

    count++;
  }

  _ctx.closePath();
  _ctx.fill();

  //
  // NOTE:
  //
  // Due to anti-aliasing, two solid triangles drawn on a Canvas
  // will often have a visible seam along the diagonal.
  //
  // A workaround for this is to also stroke the path. While this
  // does prevent the seam from being visible it does also interact
  // how the intersection of a diagonal line with another line
  // is rendered.
  //
  // But it sure beats drawing every triangle three times which is
  // the other suggested workaround!
  //
  // Uncertain how it will work with pre-vertex colors.
  //
  // See also:
  //
  //   * <https://github.com/whatwg/html/issues/4485>
  //
  //   * <https://stackoverflow.com/questions/29207924/canvas-fill-spaces-between-shapes-antialias#29209483>
  //
  _ctx.stroke(); // Part of workaround for seam between solid triangles on Canvas.

}


// TODO: Refactor into a class.
var _wacc_function;

function render_wacc(wacc_module) {

  _canvas = document.getElementById('canvas');
  _ctx = canvas.getContext('2d');

  console.debug("loaded WACC module:", wacc_module);

  if (!("wacc" in wacc_module.instance.exports)) {
    console.log("not a valid WACC module:", wacc_module);
    return;
  }

  _enable_animation = true;
  _enable_extra_debug_log = true;

  _reset_state();

  _wacc_function = wacc_module.instance.exports.wacc;

  draw_frame(MS_PER_FRAME);

}


const MAX_TRIANGLES_PER_CALL = 256; // TODO: Figure out a reasonable value.

var _is_first_triangle_in_file;
var _is_frame_mode_enabled;

function _reset_state() {
  _is_first_triangle_in_file = true;
  _is_frame_mode_enabled = false;
}


function _accumulate_triangles() {

  var accumulated_triangles = [];

  while(true) {
    var current_triangle = _get_next_triangle(_wacc_function);

    if (_enable_extra_debug_log) {
      console.debug("got triangle: ", current_triangle);
      console.debug("is_frame_end: ", current_triangle.is_frame_end);
    }

    if (_is_first_triangle_in_file) {
      _is_first_triangle_in_file = false;

      if (current_triangle.is_frame_end) {
        _is_frame_mode_enabled = true;
        continue;
      }
    }

    if (_is_frame_mode_enabled && current_triangle.is_frame_end) {
      break;
    }

    accumulated_triangles.push(current_triangle);

    if (!_is_frame_mode_enabled) {
      break;
    };

    if (accumulated_triangles.length >= MAX_TRIANGLES_PER_CALL) {
      console.warn("Hit MAX_TRIANGLES_PER_CALL limit of %d.", MAX_TRIANGLES_PER_CALL);
      break;
    }
  }

  return accumulated_triangles;

}


const MS_PER_FRAME = 1000;
var _previous_draw_time = 0;

function draw_frame(time_stamp) {

  if (!_enable_animation) {
    return;
  }

  if ((time_stamp - _previous_draw_time) >= MS_PER_FRAME) {
    // TODO: Retrieve the next triangle in advance.

    var accumulated_triangles = _accumulate_triangles();

    // Note: I'm still not sure this is the best default but
    //       it matches the non-web viewer.
    _ctx.clearRect(0,0,255,255);

    for (current_triangle of accumulated_triangles) {
      draw_triangle(current_triangle);

      // Note: Alternate workaround for seam between solid triangles on Canvas
      //       is to draw each triangle another two times here..

    }

    _previous_draw_time = time_stamp;

    _enable_extra_debug_log = false; // Only show extra logging for 1st triangle/frame.
  }

  if (_enable_animation) {
    window.requestAnimationFrame(draw_frame);
  }

}


var _enable_animation = true;
function stop() {
  _enable_animation = false;
}


function load_wacc_from_bytes(wasm_bytes) {

  WebAssembly.instantiate(wasm_bytes).then(wasm_module => render_wacc(wasm_module));

}


function load_wacc(the_url) {
  console.log("fetching WACC file: " + the_url);

  fetch(the_url)
  .then(response => response.arrayBuffer())
  .then(wasm_bytes => load_wacc_from_bytes(wasm_bytes));

}
