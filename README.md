## WebAssembly Calling Card (WACC) Web Viewer

Example implementation of an HTML Canvas-based WACC renderer/viewer.

See: <https://wacc.rancidbacon.com/>
